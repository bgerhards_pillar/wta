import React, { Component } from 'react';

export default class ColumnHeader extends Component {

  render(props) {
      const columnTitle = this.props.columnTitle;
    return (
        <div className="row">
            <div className="col-md-10">{columnTitle}</div>
        </div>
    );
  }
}