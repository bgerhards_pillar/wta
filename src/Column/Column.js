import React, { Component } from 'react';
import ColumnHeader from './ColumnHeader/ColumnHeader';
import ColumnBody from './ColumnBody/ColumnBody';

export default class Column extends Component {

  render(props) {
    const columnTitle = this.props.columnTitle;
    return (
        <div>
            <div className='col-md-4'>
              <ColumnHeader columnTitle={columnTitle} buttonClick></ColumnHeader>
              <ColumnBody buttonClicks={[this.handleButtonClick, this.handleButtonClick2, this.handleButtonClick3]}></ColumnBody>
            </div>
        </div>
    );
  }

  handleButtonClick(){
    console.log("hello world");
    alert(1);
  }

  handleButtonClick2(){
    console.log("Hello person");
    alert('2');
  }

  handleButtonClick3(){
    console.log("Hello persons");
    alert('3');
  }
}