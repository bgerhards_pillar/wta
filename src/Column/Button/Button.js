import React, { Component } from 'react';

export default class Button extends Component {
  render(props) {
    const className = this.props.className;
    const type = this.props.type;
    const children = this.props.children;
    const onClick = this.props.onClick;

    return (
        <div>
            <button className={className} type={type} onClick={onClick}>{children}</button>
        </div>
    );
  }
}