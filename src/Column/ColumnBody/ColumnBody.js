import React, { Component } from 'react';
import Button from '../Button/Button';

export default class ColumnBody extends Component {
  render(props) {
    return (
        <div>
            <div>
              <Button className="btn btn-primary btn-sm" type="submit" onClick={this.props.buttonClicks[0]}>Button</Button>
              <Button className="btn btn-success btn-sm" type="submit" onClick={this.props.buttonClicks[1]}>Button</Button>
              <Button className="btn btn-warning btn-sm" type="submit" onClick={this.props.buttonClicks[2]}>Button</Button>
            </div>
        </div>
    );
  }
}