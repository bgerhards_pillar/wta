import React, { Component } from 'react';
import Column from '../Column/Column';

export default class AppBody extends Component {

  render() {
      const columnTitles = ['Column 1', 'Second Column', 'The Third Column'];
    return (
        <div className="container">
        <div className='row'>
            {columnTitles.map((column, i) => {
                return <Column columnTitle={column} key={i}></Column>
            })}
        </div>
        </div>
    );
  }
}